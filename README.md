# Bootloader for DK08 smartwatch running NuttX

This repo is forked from https://github.com/fanoush/nrf52-legacy-bootloader and adds the following changes to increase safety (avoid bricking)
while maximizing low-power usage:

- Configure touch button with SENSE capability (allows wakeup from sleep)
- Enter DFU mode (don't boot app) if reached bootloader due to watchdog reset
- If starting watch connected to charger, enter DFU as well
- Do not require pressing button during boot to upgrade firmware on watch (only affects original firmware)
- Start a 10s watchdog right before running application. The watchdog will be **paused** during sleep so it is possible to sleep
  for long periods of time without having to wakeup to pet the watchdog. Having the touch button set to SENSE is critical
  to ensure watch is not bricked during sleep without any wakeup capability.

This bootloader still allows the original firmware to be used.

## Installation

On an untouched watch with original firmware, you need to first replace the stock bootloader with bootloader from fanoush. Follow
these instructions: https://github.com/fanoush/ds-d6/tree/master/espruino/DFU/DK08

Once this is done, upgrading the bootloader to this one for NuttX is much easier since how you can do it via DFU.
To do so, first build this bootloader as a DFU package.

1. Install the toolchain linked below (this example assumed it is decompressed in a `toolchain` subdirectory)
2. Install Adafruit nrfutil: https://github.com/adafruit/Adafruit_nRF52_nrfutil
3. Run the following:

     make BOARD=DK08 GNU_INSTALL_ROOT=toolchain/gcc-arm-none-eabi-8-2019-q3-update VERBOSE=1 dfu

  This will generate a .zip file inside `_build` subdirectory.

4. Install bootloader using nRFConnect app: https://play.google.com/store/apps/details?id=no.nordicsemi.android.mcp

After this step you will be able to again flash stock firmware to watch or the NuttX based one.

---

# nrf52-legacy-bootloader

SDK11 Nordic legacy DFU OTA bootloader

### Getting started

This needs slightly patched Nordic SDK11, run get-sdk11.sh and hopefully it should be downloaded, extracted into SDK11 folder and patched with sdk11.patch. If it fails then do it by hand :-) Needs `wget`, `unzip`, `patch` and `dos2unix` to convert text files to unix format.

Then just type `make`, output is in `_build/nrf52832_xxaa_s132_<address>.hex`.

To build for specific board (e.g. F07)  run it like `make BOARD=F07` and the output should be in `_build/F07_s132_<address>.hex`.

### Changes

You can change some variables in Makefile

- BOOTLOADER_START - bootloader start address, one of `78000`, `7a000` or `7b000`
- -DFEED_WATCHDOG - will keep feeding watchdog if it is enabled at bootloader entry
- -DDISABLE_BUTTONLESS_DFU - trims down the size, disables Buttonless DFU
- -DDISABLE_DFU_APPCHECK - trims down the size, disables check for application and device version in dfu package, softdevice version is still checked
- -DSMALL_INTERRUPT_VECTORS - trims down size, make initial interrupt vectors area size smaller (from 0x400 to 0x100)
- GNU_INSTALL_ROOT - location of ARM gcc compiler, I have used [gcc-arm-none-eabi-8-2019-q3-update](https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-rm/downloads/8-2019q3-update) which is latest gcc 8, newer gcc 9 produced bigger code for me
